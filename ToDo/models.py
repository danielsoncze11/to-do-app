from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Todo(models.Model):
    """"class which creates model with to-dos and its attributes"""

    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=60)
    added_date = models.DateTimeField(default=timezone.now)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.text
