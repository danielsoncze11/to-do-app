from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home-todo'),
    path('user/<str:username>', views.UserTodoListView.as_view(), name='user-todo'),
    path('user/create/', views.create_todo, name='create-todo'),
    path('deleteTodo/<todo_id>/', views.delete_todo, name='delete-todo'),
    path('complete/<todo_id>', views.complete_todo, name='complete-todo'),
]
