import os
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import User
from .models import Todo
from django.views.generic import ListView

def home(request):
    """"function to display the home page with all users"""

    context = {'users': User.objects.all()}
    return render(request, 'ToDo/home.html', context)

class UserTodoListView(ListView):
    """"class view to view respective user to-do's"""

    model = Todo
    template_name = 'ToDo/user_Todo.html'

    def get_queryset(self):
        """"to filter only user to-do's and to show completed first"""

        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return Todo.objects.filter(author=user).order_by('completed')

def create_todo(request):
    """"function to create a new to-do"""

    if (request.method == 'POST'):
        new_todo = Todo(text=request.POST['text'], author=request.user)
        new_todo.save()
        user = str(request.user)
        return redirect(os.path.join('/user/', user))
    else:
        return render(request, 'ToDo/create_todo.html')

def delete_todo(request, todo_id):
    """"function to delete to-do"""

    todo_to_delete = Todo.objects.get(id=todo_id)
    todo_to_delete.delete()
    user = str(request.user)
    return redirect(os.path.join('/user/', user))



def complete_todo(request, todo_id):
    """"function to either complete task or make it not complete again"""

    todo = Todo.objects.get(pk=todo_id)
    if not todo.completed:
        todo.completed = True
    else:
        todo.completed = False
    todo.save()
    user = str(request.user)
    return redirect(os.path.join('/user/', user))
