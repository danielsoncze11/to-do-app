from django.urls import path
from .views import (PostListAPIView,
                    PostRetrieveAPIView,
                    PostUpdateAPIView,
                    PostDeleteAPIView,
                    )

urlpatterns = [
    path('', PostListAPIView.as_view(), name='home-todo-api'),
    path('text/<text>', PostRetrieveAPIView.as_view(), name='detail-todo-api'),
    path('text/<text>/update', PostUpdateAPIView.as_view(), name='update-todo-api'),
    path('text/<text>/delete', PostDeleteAPIView.as_view(), name='delete-todo-api'),
]