from rest_framework.generics import (
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    )

from rest_framework.permissions import (
    IsAuthenticated,
    )

from ToDo.models import Todo
from .serializers import (
    PostSerializer,
    DetailSerializer,
    )

class PostListAPIView(ListAPIView):
    queryset = Todo.objects.all()
    serializer_class = PostSerializer
    filter_fields = ('author',)
    permission_classes = (IsAuthenticated,)

class PostRetrieveAPIView(RetrieveAPIView):
    queryset = Todo.objects.all()
    serializer_class = DetailSerializer
    lookup_field = 'text'

class PostUpdateAPIView(UpdateAPIView):
    queryset = Todo.objects.all()
    serializer_class = DetailSerializer
    lookup_field = 'text'

class PostDeleteAPIView(DestroyAPIView):
    queryset = Todo.objects.all()
    serializer_class = PostSerializer
    lookup_field = 'text'
    permission_classes = (IsAuthenticated,)