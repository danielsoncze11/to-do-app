from rest_framework.serializers import ModelSerializer, HyperlinkedIdentityField

from ToDo.models import Todo

class PostSerializer(ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='todo-api:detail-todo-api',
        lookup_field='text'
    )
    delete_url = HyperlinkedIdentityField(
        view_name='todo-api:delete-todo-api',
        lookup_field='text'
    )

    class Meta:
        model = Todo
        fields = [
            'url',
            'author',
            'text',
            'added_date',
            'completed',
            'delete_url'
        ]

class DetailSerializer(ModelSerializer):
    class Meta:
        model = Todo
        fields = [
            'text',
            'added_date',
            'completed',
        ]
