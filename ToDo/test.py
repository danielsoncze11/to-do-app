from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse, resolve
from ToDo.views import home, UserTodoListView, create_todo, complete_todo

class TestUrls(SimpleTestCase):
    """"class to test URLs"""

    def test_home(self):
        """"function to test todo_home url """

        url = reverse('home-todo')
        self.assertEquals(resolve(url).func, home)

    def test_create_todo(self):
        """"function to test create_todo url """

        url = reverse('create-todo')
        self.assertEquals(resolve(url).func, create_todo)

    def test_list_view(self):
        """"function to test todo_list_view url """

        url = reverse('user-todo', args=['Dan'])
        self.assertEquals(resolve(url).func.view_class, UserTodoListView)

    def test_complete_view(self):
        """"function to test complete_view url """

        url = reverse('complete-todo', args=[1])
        self.assertEquals(resolve(url).func, complete_todo)


class TestViews(TestCase):
    """"class to test URLs"""

    def test_home_list(self):
        """"function to test todo_home page that it can be accessed and template is loaded"""

        client = Client()
        response = client.get(reverse('home-todo'))

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'ToDo/home.html')

