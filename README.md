**To Do APP**
- App for multiple users to create To-do list and track the completion
- User can register and create own To-do list
- To-do's can be marked as complete
- To-do list of each user can be viewed

**Setup:**
1.  Create virtual environment `virtualenv your_environment_name`
2.  Run virtual environment `your_environment_name\Scripts\activate`
3.  Clone this repo `git clone https://gitlab.com/danielsoncze11/to-do-app.git`
4.  `pip install -r requirements.txt`
5.  `python manage.py runserver`

**Super User:**
name - Dan
pw - dan11